/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unknownfrommatrix;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author martinhajny
 */
public class UnknownFromMatrix {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter size of matrix, digit n: ");
        int primaryMatSize = sc.nextInt();
        int [] [] primaryMat = new int [primaryMatSize] [primaryMatSize+1];     // define primary matrix
        
        System.out.println("Type of entered matrix is " + primaryMatSize+"x"+primaryMatSize);
        System.out.println("Now enter each digit separately, after every digit PRESS ENTER");
        
        for (int i = 0; i < primaryMat.length; i++) {                           // enter all values to primary matrix
            for (int j = 0; j < primaryMat[i].length; j++) {
                int tmp = sc.nextInt();
                primaryMat[i][j] = tmp;
            }
            System.out.println("Next row");
        }
        
        printMat(primaryMat);
        
        double primaryD = Determinant.determinantNxN(primaryMat, primaryMatSize);   // determinant of primary matrix
        
        //System.out.println("Determinant of primary matrix is: " + primaryD);
        
        ArrayList solution = new ArrayList();                                   // arraylist for solutions
        
        for (int i = 0; i < primaryMat.length; i++) {
            
            int [][] subMat = new int [primaryMat.length][primaryMat.length];   // initialize sub-matrix for every unknown

                for (int j = 0; j < primaryMat.length; j++) {                   // copy unknows from primary matrix
                    for (int k = 0; k < primaryMat.length; k++) {
                        subMat[j][k] = primaryMat[j][k];
                    }
                 }
                
                for (int j = 0; j < primaryMat.length; j++) {                   // rewrite results of each row from primary matrix to correct position
                     subMat[j][i] = primaryMat[j][primaryMat.length];
                 }
            
            // printMat(subMat);
            
            double subMatD = Determinant.determinantNxN(subMat, subMat.length); // find determinant of sub-matrix
            
            //System.out.println((i+1) + ". unknown is " + (subMatD/primaryD));
            
            solution.add(subMatD/primaryD);                                     // write solution of each unknown to array list
        }
        
        System.out.println("K = {" + solution +"}");                            // print solution
        
    }
    
    public static void printMat(int mat[][]){                                   // print matrix
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                System.out.print(mat[i][j] + ", ");
            }
            System.out.println("");
        }
    }
    
}
