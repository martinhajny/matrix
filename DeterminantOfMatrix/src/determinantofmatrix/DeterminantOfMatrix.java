/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package determinantofmatrix;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author martinhajny
 */
public class DeterminantOfMatrix {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter size of matrix, digit n: ");
        int primaryMatSize = sc.nextInt();
        int [] [] primaryMatrix = new int [primaryMatSize] [primaryMatSize];    // define primary matrix
        System.out.println("Type of entered matrix is " + primaryMatSize+"x"+primaryMatSize);
        System.out.println("Now enter each digit separately, after every digit PRESS ENTER");
        for (int i = 0; i < primaryMatrix.length; i++) {                        // enter all values to primary matrix
            for (int j = 0; j < primaryMatrix[i].length; j++) {
                int tmp = sc.nextInt();
                primaryMatrix[i][j] = tmp;
            }
        }
        
        System.out.println("Matrix looks like this:");
        for (int i = 0; i < primaryMatrix.length; i++) {                        // print the primary matrix
            for (int j = 0; j < primaryMatrix[i].length; j++) {
                System.out.print(primaryMatrix[i][j] + ", ");
            }
            System.out.println("");
        }
        
        System.out.println("Determinant is: " + determinantNxN(primaryMatrix, primaryMatSize));
    }
    
    public static int determinantNxN(int mat[][],int size) {        // mat = entered matrix; size = size of entered matrix
        int D = 0;
        switch (size) {
            case 1:                                                 // 1x1 m. has determinant the only digit
                D = mat[0][0];
                break;
            case 2:                                                 // 2x2 m. is calculated by simple method
                D = mat[0][0]*mat[1][1] - mat[1][0]*mat[0][1];
                break;
            default:{                                               // else, let the recursion begin
                for(int i=0;i<size;i++) {                           // loop throught all numbers from mat[0]

                    int [][] subMat = new int[size-1][size-1];      // make smaller matrix

                    for(int j=0;j<size-1;j++) {                     // loop for filling sub matrix, row of both matrices

                    int col=0;                                      //                              column of sub matrix

                        for(int k=0;k<size;k++) {                   // loop for filling sub matrix, column of entering matrix
                            if(i == k){
                                continue;                           // unwanted index, skip it
                            }
                            subMat[j][col++] = mat[j+1][k];         // fill the sub matrix
                        }                                           // end of for k
                    }                                               // end of for j
                    if (i%2==0){                                    // ADD or DEDUCT result of submatrix to final determinant
                        D+=mat[0][i] * determinantNxN(subMat,size-1);
                    }
                    else{
                        D-=mat[0][i] * determinantNxN(subMat,size-1);
                    }
                }                                                   // end of for i
            }
            break;
        }                                                           // end of switch
        return D;                                                   // get determinant
    }
}